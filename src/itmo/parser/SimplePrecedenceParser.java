package itmo.parser;

import itmo.parser.grammar.Grammar;
import itmo.parser.grammar.GrammarSymbol;
import itmo.parser.grammar.GrammarSymbolsString;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Supplier;

public class SimplePrecedenceParser {
    private Grammar grammar;
    private BiFunction<GrammarSymbol, GrammarSymbol, WWPrecedenceRelationship> relationshipSupplier;

    private Supplier<? extends GrammarSymbol> scanner;
    private State state = State.IN_PROGRESS;
    private LinkedList<GrammarSymbol> stack;
    private GrammarSymbol nextSymbol;

    public SimplePrecedenceParser(
            Grammar grammar,
            BiFunction<GrammarSymbol, GrammarSymbol, WWPrecedenceRelationship> relationshipSupplier

    ) {
        this.grammar = grammar;
        this.relationshipSupplier = relationshipSupplier;
    }

    public boolean parse(Supplier<? extends GrammarSymbol> scanner) {
        this.scanner = scanner;

        initializeParse();
        nextSymbol = scanner.get();

        while (state == State.IN_PROGRESS) {
            WWPrecedenceRelationship relationship = stack.size() < 1 ? WWPrecedenceRelationship.POSSIBILITY
                            : Objects.isNull(nextSymbol) ? WWPrecedenceRelationship.COMPLETE
                            : relationshipSupplier.apply(stack.peekLast(), nextSymbol);

            System.out.print(String.format("stack: %s, next: %s, rel: %s ", stack, nextSymbol, relationship));

            switch (relationship) {
                case NONE:
                    error();
                    break;

                case POSSIBILITY:
                case PARTIALY_COMPLETE:
                    shift();
                    break;

                case COMPLETE:
                    reduce();
                    break;
            }

            System.out.println();
        }

        return state == State.ACCEPT;
    }

    private void initializeParse() {
        state = State.IN_PROGRESS;
        stack = new LinkedList<>();
    }

    private void error() {
        state = State.ERROR;
    }

    private void shift() {
        System.out.print("action: shift ");

        stack.add(nextSymbol);
        nextSymbol = scanner.get();
    }

    private void reduce() {
        List<GrammarSymbol> fromMarker = stack.subList(findMarker() + 1, stack.size());
        if (fromMarker.size() < 1) {
            error();
            return;
        }

        GrammarSymbolsString string = new GrammarSymbolsString(fromMarker);
        fromMarker.clear();

        Optional<GrammarSymbol> maybeLeftPart = grammar.getLeftPartFor(string);
        if (maybeLeftPart.isPresent()) {
            System.out.print(String.format("action: reduce %s -> %s", maybeLeftPart.get(), string));

            stack.add(maybeLeftPart.get());
            if (stack.peekLast().equals(grammar.getGoalSymbol()) && Objects.isNull(nextSymbol)) {
                System.out.println("accept");
                accept();
            }
        } else {
            error();
        }
    }

    private void accept() {
        state = State.ACCEPT;
    }

    private int findMarker() {
        if (stack.size() < 2) {
            return -1;
        }

        int i = stack.size() - 2;
        while (i >= 0 &&
               relationshipSupplier.apply(stack.get(i), stack.get(i + 1)) != WWPrecedenceRelationship.POSSIBILITY
        ) { i--; }

        return i;
    }

    private enum State {
        IN_PROGRESS, ERROR, ACCEPT
    }
}
