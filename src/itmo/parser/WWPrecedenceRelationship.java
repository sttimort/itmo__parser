package itmo.parser;

public enum WWPrecedenceRelationship {
    POSSIBILITY, PARTIALY_COMPLETE, COMPLETE, NONE
}
