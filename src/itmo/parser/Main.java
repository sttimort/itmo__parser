package itmo.parser;

public class Main {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: parser <input string>");
            return;
        }

        SimplePrecedenceParser parser = new SimplePrecedenceParser(
                Configuration.grammar,
                Configuration.relationships::get
        );

        try {
            boolean matches = parser.parse(new SimpleScanner(args[0]));
            System.out.println(matches ? "matches" : "doesn't match");
        } catch (SimpleScanner.InvalidSymbolException ex) {
            System.err.println(ex.getMessage());
        }
    }
}
