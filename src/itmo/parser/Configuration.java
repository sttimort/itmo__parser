package itmo.parser;

import itmo.parser.grammar.Grammar;
import itmo.parser.grammar.GrammarSymbol;
import itmo.parser.grammar.GrammarSymbolsString;
import itmo.parser.grammar.Rule;
import itmo.parser.util.BiMap;

import java.util.Arrays;

import static itmo.parser.WWPrecedenceRelationship.*;
import static itmo.parser.WWPrecedenceRelationship.COMPLETE;
import static itmo.parser.grammar.LexicalCategory.*;
import static itmo.parser.grammar.SyntacticCategory.*;
import static itmo.parser.grammar.SyntacticCategory.D;

public class Configuration {
    public static Grammar grammar = new Grammar(G, Arrays.asList(
            new Rule(G, GrammarSymbolsString.of(B)),

            new Rule(B, GrammarSymbolsString.of(B, o, W)),
            new Rule(B, GrammarSymbolsString.of(W)),

            new Rule(C, GrammarSymbolsString.of(C, a, D)),
            new Rule(C, GrammarSymbolsString.of(H)),

            new Rule(D, GrammarSymbolsString.of(n, D)),
            new Rule(D, GrammarSymbolsString.of(o_par, H, c_par)),
            new Rule(D, GrammarSymbolsString.of(t)),
            new Rule(D, GrammarSymbolsString.of(f)),

            new Rule(W, GrammarSymbolsString.of(C)),
            new Rule(H, GrammarSymbolsString.of(D))
    ));

    public static BiMap<GrammarSymbol, GrammarSymbol, WWPrecedenceRelationship> relationships = new BiMap<>(NONE);

    static {
        relationships.set(B,       o,      PARTIALY_COMPLETE);
        relationships.set(W,       o,      COMPLETE);
        relationships.set(C,       o,      COMPLETE);
        relationships.set(C,       a,      PARTIALY_COMPLETE);
        relationships.set(D,       o,      COMPLETE);
        relationships.set(D,       a,      COMPLETE);
        relationships.set(D,       c_par,  COMPLETE);
        relationships.set(H,       o,      COMPLETE);
        relationships.set(H,       a,      COMPLETE);
        relationships.set(H,       c_par,  PARTIALY_COMPLETE);
        relationships.set(o,       W,      PARTIALY_COMPLETE);
        relationships.set(o,       C,      POSSIBILITY);
        relationships.set(o,       D,      POSSIBILITY);
        relationships.set(o,       H,      POSSIBILITY);
        relationships.set(o,       n,      POSSIBILITY);
        relationships.set(o,       o_par,  POSSIBILITY);
        relationships.set(o,       t,      POSSIBILITY);
        relationships.set(o,       f,      POSSIBILITY);
        relationships.set(a,       D,      PARTIALY_COMPLETE);
        relationships.set(a,       n,      POSSIBILITY);
        relationships.set(a,       o_par,  POSSIBILITY);
        relationships.set(a,       t,      POSSIBILITY);
        relationships.set(a,       f,      POSSIBILITY);
        relationships.set(n,       D,      PARTIALY_COMPLETE);
        relationships.set(n,       n,      POSSIBILITY);
        relationships.set(n,       o_par,  POSSIBILITY);
        relationships.set(n,       t,      POSSIBILITY);
        relationships.set(n,       f,      POSSIBILITY);
        relationships.set(o_par,   D,      POSSIBILITY);
        relationships.set(o_par,   H,      PARTIALY_COMPLETE);
        relationships.set(o_par,   n,      POSSIBILITY);
        relationships.set(o_par,   o_par,  POSSIBILITY);
        relationships.set(o_par,   t,      POSSIBILITY);
        relationships.set(o_par,   f,      POSSIBILITY);
        relationships.set(c_par,   o,      COMPLETE);
        relationships.set(c_par,   a,      COMPLETE);
        relationships.set(c_par,   c_par,  COMPLETE);
        relationships.set(t,       o,      COMPLETE);
        relationships.set(t,       a,      COMPLETE);
        relationships.set(t,       c_par,  COMPLETE);
        relationships.set(f,       o,      COMPLETE);
        relationships.set(f,       a,      COMPLETE);
        relationships.set(f,       c_par,  COMPLETE);
    }
}
