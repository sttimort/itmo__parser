package itmo.parser.grammar;

import java.util.*;

public class GrammarSymbolsString {
    public static GrammarSymbolsString of(GrammarSymbol...symbols) {
        return new GrammarSymbolsString(Arrays.asList(symbols));
    }

    private List<GrammarSymbol> innerSequence = new LinkedList<>();

    public GrammarSymbolsString(Collection<GrammarSymbol> symbols) {
        innerSequence.addAll(symbols);
    }

    @Override
    public int hashCode() {
        return innerSequence.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof GrammarSymbolsString) &&
                innerSequence.equals(((GrammarSymbolsString)obj).innerSequence);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        innerSequence.forEach(sb::append);

        return sb.toString();
    }
}
