package itmo.parser.grammar;

import itmo.parser.grammar.GrammarSymbol;
import itmo.parser.grammar.GrammarSymbolsString;

public class Rule {
    private GrammarSymbol left;
    private GrammarSymbolsString right;

    public Rule(GrammarSymbol left, GrammarSymbolsString right) {
        this.left = left;
        this.right = right;
    }

    public GrammarSymbol getLeft() {
        return left;
    }

    public GrammarSymbolsString getRight() {
        return right;
    }
}
