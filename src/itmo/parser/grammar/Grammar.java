package itmo.parser.grammar;

import java.util.*;

public class Grammar {
    private Map<GrammarSymbol, Integer> leftPartToRuleIndexMap = new HashMap<>();
    private Map<GrammarSymbolsString, Integer> rightPartToRuleIndexMap = new HashMap<>();

    private GrammarSymbol goalSymbol;
    private List<Rule> rules;

    public Grammar(GrammarSymbol goalSymbol, Collection<Rule> rules) {
        this.goalSymbol = goalSymbol;
        this.rules = new ArrayList<>(rules.size());

        int i = 0;
        for (Rule rule : rules) {
            leftPartToRuleIndexMap.put(rule.getLeft(), i);
            rightPartToRuleIndexMap.put(rule.getRight(), i);
            this.rules.add(rule);
            i++;
        }
    }

    public GrammarSymbol getGoalSymbol() {
        return goalSymbol;
    }

    public Optional<GrammarSymbol> getLeftPartFor(GrammarSymbolsString rightPart) {
        return Optional.ofNullable(rightPartToRuleIndexMap.get(rightPart))
                .map(i -> rules.get(i))
                .map(Rule::getLeft);
    }
}
