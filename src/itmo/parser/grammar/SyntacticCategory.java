package itmo.parser.grammar;

import itmo.parser.grammar.GrammarSymbol;

public enum SyntacticCategory implements GrammarSymbol {
    G, B, W, C, D, H
}
