package itmo.parser.grammar;

import itmo.parser.grammar.GrammarSymbol;

public enum LexicalCategory implements GrammarSymbol {
    o, a, n, t, f, o_par, c_par
}
