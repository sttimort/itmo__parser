package itmo.parser.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class BiMap<K1, K2, V> {
    private final Map<K1, Map<K2, V>> map = new HashMap<>();
    private V defaultValue;

    public BiMap(V defaultValue) {
        this.defaultValue = defaultValue;
    }

    public <inK1 extends K1, inK2 extends K2> V get(inK1 k1, inK2 k2) {
        return Optional.ofNullable(map.get(k1))
                .map(innerMap -> innerMap.get(k2))
                .orElse(defaultValue);
    }

    public <inV extends V, inK1 extends K1, inK2 extends K2> void set(inK1 k1, inK2 k2, inV value) {
        if (!map.containsKey(k1)) {
            map.put(k1, new HashMap<>());
        }

        map.get(k1).put(k2, value);
    }
}
