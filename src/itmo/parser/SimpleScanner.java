package itmo.parser;

import itmo.parser.grammar.LexicalCategory;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static itmo.parser.grammar.LexicalCategory.*;

public class SimpleScanner implements Supplier<LexicalCategory> {
    private final Map<Character, LexicalCategory> charToCategoryMap = Stream.of(
            new SimpleImmutableEntry<>('(', o_par),
            new SimpleImmutableEntry<>(')', c_par),
            new SimpleImmutableEntry<>('o', o),
            new SimpleImmutableEntry<>('a', a),
            new SimpleImmutableEntry<>('n', n),
            new SimpleImmutableEntry<>('t', t),
            new SimpleImmutableEntry<>('f', f)
    ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    private int i = 0;
    private String input;

    public SimpleScanner(String input) {
        this.input = input;
    }

    @Override
    public LexicalCategory get() {
        if (i >= input.length()) {
            return null;
        }

        Character c = input.charAt(i++);
        return Optional.ofNullable(charToCategoryMap.get(c))
                .orElseThrow(() -> new InvalidSymbolException(c));
    }

    public static class InvalidSymbolException extends RuntimeException {
        public InvalidSymbolException(Character c) {
            super(String.format("Character %c is invalid", c));
        }
    }
}
